require gammaspc
require essioc
require iocmetadata

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P", "MBL-030RFC:")
epicsEnvSet("R1", "RFS-VacPS-110:")
epicsEnvSet("IP1", "mbl3-rf1-ip1.tn.esss.lu.se:23")
epicsEnvSet("PORT1", "GAMMA1")

epicsEnvSet("R2", "RFS-VacPS-120:")
epicsEnvSet("IP2", "mbl3-rf1-ip2.tn.esss.lu.se:23")
epicsEnvSet("PORT2", "GAMMA2")


iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh", "R=$(R1), IP=$(IP1), PORT=$(PORT1)")
iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh", "R=$(R2), IP=$(IP2), PORT=$(PORT2)")

pvlistFromInfo("ARCHIVE_THIS","$(IOCNAME):ArchiverList")

